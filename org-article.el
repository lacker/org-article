;;; org-article

;; Author: Leo Ackermann
;; Version: 0.1
;; URL: TODO
;; Package-Requires: TODO
;; License: GPLv3


;;; Commentary:
;; TODO

;;; Code:

(require 'org)

 ;; Pretty export

(defun org-article-def (s backend info)
  (replace-regexp-in-string "\\[Definition\\] \\(.*?\\)[\n]\\([[:ascii:]]*?\\)[\n][-]\\{3\\}" "\\\\begin{definition}[\\1]\n\\2\n\\\\end{definition}" s))


(add-to-list 'org-export-filter-body-functions
             'org-article-def)

 ;; Fontification

(font-lock-add-keywords 'org-mode
                        '(("\\(\\[Definition\\]\\) \\(.*?\\)[\n]\\([[:ascii:]]*?\\)[\n]\\([-]\\{3\\}\\)" (1 'org-checkbox) (2 'org-drawer) (4 'org-checkbox)))
                        )

(provide 'org-article)
;;; org-article.el ends here
